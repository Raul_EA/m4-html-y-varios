let para = document.createElement("p");
document.body.appendChild(para);

const myJson = '[\n' +
    '  {\n' +
    '    "author": "Samuel Beckett",\n' +
    '    "language": "French, English",\n' +
    '    "title": "Molloy, Malone Dies, The Unnamable, the trilogy",\n' +
    '    "year": 1952\n' +
    '  },\n' +
    '  {\n' +
    '    "author": "Miguel de Cervantes",\n' +
    '    "language": "Spanish",\n' +
    '    "title": "Don Quijote De La Mancha",\n' +
    '    "year": 1610\n' +
    '  },\n' +
    '  {\n' +
    '    "author": "William Shakespeare",\n' +
    '    "language": "English",\n' +
    '    "title": "King Lear",\n' +
    '    "year": 1608\n' +
    '  },\n' +
    '  {\n' +
    '    "author": "Federico Garc\u00eda Lorca",\n' +
    '    "language": "Spanish",\n' +
    '    "title": "Gypsy Ballads",\n' +
    '    "year": 1928\n' +
    '  }\n' +
    ']';

const myObj = JSON.parse(myJson)
myObj.sort((a, b)=>a.year - b.year);
console.log(myObj);
/*
const years=[];
for(let i=0;i<myObj.length;i++){
    years.push(parseInt(myObj[i]['year']));
}
years.sort((a,b)=>a-b)
const myObjF=[];
for(let i=0;i<years.length;i++){
    for(let j=0;j<myObj.length;j++){
        if(myObj[j]['year']==years[i]){
            myObjF.push(myObj[j]);
        }
    }
}
console.log(myObjF);
*/