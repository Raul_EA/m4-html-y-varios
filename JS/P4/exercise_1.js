function highlight(){
    document.body.style.backgroundColor="yellow";
    for (let j = 0; j < 2; j++) {
        document.getElementsByTagName("p")[j].style.color="red";
    }
    for (let j = 0; j < 6; j++) {
        document.getElementsByTagName("strong")[j].style.color="green";
        document.getElementsByTagName("strong")[j].style.fontSize="64px";
    }
}
function return_normal(){
    document.body.style.backgroundColor="white";
    for (let j = 0; j < 2; j++) {
        document.getElementsByTagName("p")[j].style.color="black";
    }
    for (let j = 0; j < 6; j++) {
        document.getElementsByTagName("strong")[j].style.color="black";
        document.getElementsByTagName("strong")[j].style.fontSize="medium";
    }
}