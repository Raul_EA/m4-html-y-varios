let row = parseInt(prompt("Introduce the number of rows: "));
let col = parseInt(prompt("Introduce the number of columns: "));
var tab = document.createElement("table");
var tabBody = document.createElement("tbody");

for(var i=0;i<=row;i++){
    if(parseInt(i)==0){
        var hilera = document.createElement("tr");
        for(var j=1;j<=col;j++){
            let celda = document.createElement("th");
            celda.innerHTML="&nbsp;&nbsp;Column "+j+"&nbsp;&nbsp;";
            hilera.appendChild(celda);
            celda.style.borderWidth="thin";
            celda.style.color="blue";
            celda.style.margin="10%";
        }
        tabBody.appendChild(hilera);
    }
    else {
        var hilera = document.createElement("tr");
        for (var j = 0; j < col; j++) {
            let celda = document.createElement("td");
            celda.innerHTML=parseInt((Math.random()*11));
            hilera.appendChild(celda);
            celda.style.textAlign="center";
            celda.style.color="green";
        }
        tabBody.appendChild(hilera);
    }
}
tab.appendChild(tabBody);
tab.setAttribute("border","2");
tab.style.borderCollapse="collapse";
document.body.appendChild(tab);
