$('#submit').click( function () {
    event.preventDefault();
    $.ajax({
        method: "POST",
        url: "exercise_4.php",
        data: {
            "num": $("#input").val()
        },
        dataType: "json",
        success: function (data) {
            let para =$("<p>")
            para.attr("id","para");
            $("body").append(para);
            console.log(data);
            $("#para").text(data);
            window.stop();
        },
        error: function (jqXHR, textStatus, error) {
            alert("Error: " + textStatus + " " + error);
        }
    });
});
