$(document).ready(function() {
    $.ajax({
        method: "GET",
        url: "https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json",
        data: {
            "id": $("#input").val(),
            "name": $("#input").val(),
            "appearance": {
                "race": $("#input").val()
            }
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            for(let a=0;a<data.length;a++){
                let para =$("<tr>")
                $("tbody").append(para)

                let fill =$("<td>")
                let b = "si"+a;
                fill.attr("id",b);
                para.append(fill)
                $("#si"+a).text(data[a].id);

                let fill2 =$("<td>")
                let c = "na"+a;
                fill2.attr("id",c);
                para.append(fill2)
                $("#na"+a).text(data[a].name);

                let fill3 =$("<td>")
                let d = "ra"+a;
                fill3.attr("id",d);
                para.append(fill3)
                if(data[a].appearance.race==null){
                    $("#ra"+a).text("null");
                }else{
                    $("#ra"+a).text(data[a].appearance.race);
                }
            }
            /*
             for (let i=0;i<data.length;i++){
                let superhero=data[i];
                let row= "<tr><td>"+superhero.id+"</td><td>"+superhero.name+"</td><td>"+superhero.appearance.race+"</td"
                $("#tableBody").append(row);
            }
            */
        }
    });
});

