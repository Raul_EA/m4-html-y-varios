$(document).ready(function () {
    var usersTable = $("#loginMain");
    $("#usersBtn").click(function () {
        $.ajax({
            method: "POST",
            url: "getUsers.php",
            dataType: "json",
            success: function (data) {
                console.log(data);
                usersTable.empty();
                /* ToDo using jQuery, write here a table to show all the users */
                var table = $("<table>").addClass("users-table");
                var headerRow = $("<tr>").append($("<th>").text("Username")).append($("<th>").text("Password"));
                table.append(headerRow);

                $.each(data, function (index, user) {
                    var userCell = $("<td>").text(user.nom);
                    var pwdCell = $("<td>").text(user.password);

                    var row = $("<tr>").append(userCell).append(pwdCell);
                    table.append(row);
                });
                usersTable.append(table);
            },

            error: function (jqXHR, texStatus, error) {
                alert("Error:" + texStatus + " " + error);
            }
        });
    });
});

