$(document).ready(function () {
    $("#logoutBtn").click(function () {
        $.ajax({
            method: "POST",
            url: "logout.php",
            dataType: "json",
            success: function (data) {
                console.log(data);
                if (data.success) {
                    $("#loginSection").show();
                    $("#logoutSection").hide();
                }
            },

            error: function (jqXHR, textStatus, error) {
                alert("Error: " + textStatus + " " + error);
            }
        });
    });
});

