<?php

session_start();
$_SESSION["active"] = false;
session_destroy();

$response = array('success' => true);

echo json_encode($response);

?>