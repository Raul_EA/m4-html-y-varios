$(document).ready(function () {
    $("#registerBtn").click(function () {
        var username = $("#user").val();
        var password = $("#password").val();
        if (validateUsername(username)) {
            $.ajax({
                method: "POST",
                url: "register.php",
                data: {
                    "username" :username,
                    "password" :password
                },
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if (data.success) {
                        $("#message").text("User registered successfully").css("color", "green");
                        location.reload();
                    } else {
                        $("#message").text("The username is already registered").css("color", "red");

                    }
                },
                error: function (jqXHR, textStatus, error) {
                    alert("Error: " + textStatus + " " + error);
                }
            });
        } else {
            $("#message").text("The username must be a valid @ies-sabadell.cat account").css("color", "red");

        }
    });
    function validateUsername(username) {
        var emailPattern = /^[^\s@]+@ies-sabadell\.cat$/;
        return emailPattern.test(username);
    }
});