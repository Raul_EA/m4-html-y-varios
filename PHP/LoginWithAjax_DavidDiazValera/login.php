<?php
session_start();
require_once 'config.php';
$_SESSION["active"] = false;
/*
** see the "Example (PDO with Prepared Statements)" in:
**     https://www.w3schools.com/php/php_mysql_prepared_statements.asp
*/

try {
    global $servername, $username, $password, $dbname;
    /* Establish the connection to the database */
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    /* set the PDO error mode to exception */
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /* ToDo: get the user and the password from the proper method (GET or POST)*/
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = $_POST["username"];
        $password = $_POST["password"];

        /* prepare and execute the query to know if the user with the given password is registered */
        $sql = $conn->prepare("SELECT * FROM users WHERE nom = :username AND password = :password");

        /* bind user */
        $sql->bindParam('username', $username, PDO::PARAM_STR);
        /* bind password */
        $sql->bindParam('password', $password, PDO::PARAM_STR);

        /* execute query */
        $sql->execute();

        $response = array();

        /* check if the user was successfully inserted */
        if ($sql->rowCount() > 0) {
            $response['success'] = true;
            $_SESSION["active"] = true;
        } else {
            $response['success'] = false;
        }

        /* retrieve the data */
        $result = $sql->fetch(PDO::FETCH_ASSOC);

        /* ToDo: return data to the AJAX request. TIP: if the user exists, $sql->rowCount() will be greater than 0 */
        header('Content-Type: application/json');
        echo json_encode($response);
    } else {
        echo "Invalid request method";
    }
    /*print_r(json_encode($sql . $e->getMessage())); */

} catch (PDOException $e) {

    $response = array('error' => $e->getMessage());

    header('Content-Type: application/json');
    echo json_encode($response);
    /*print_r(json_encode($sql . $e->getMessage()));*/
}

// close the PDO Connection
$conn = null;

?>