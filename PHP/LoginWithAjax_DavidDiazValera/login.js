function sessionActive() {
    $.ajax({
            method: "GET",
            url: "session.php",
            success: function (data) {
                console.log(data)
                if (data != "" & data) {
                    $("#loginSection").hide();
                    $("#logoutSection").show();
                }
            },
            error: function (jqXHR, textStatus, error) {
                alert("Error: " + textStatus + " " + error);
            }

        }
    )
}

$(document).ready(function () {
    $("#loginBtn").click(function () {
        $.ajax({
            method: "POST",
            url: "login.php",
            data: {
                "username": $("#user").val(),
                "password": $("#password").val()
            },
            dataType: "json",
            success: function (data) {
                /*console.log(data);*/

                if (data.success) {
                    $("#loginMain").text("You have successfully logged in").css("color", "green");
                    $("#loginSection").hide();
                    $("#logoutSection").show();
                } else {
                    $("#loginMain").text("Username and/or password incorrect").css("color", "red");
                }
            },
            error: function (jqXHR, textStatus, error) {
                alert("Error: " + textStatus + " " + error);
            }
        });
    });

    sessionActive();

});