<?php

require_once 'config.php';

try {
    global $servername, $username, $password, $dbname;
    /* Establish the connection to the database */
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    /* set the PDO error mode to exception */
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /* Get the username and password from the POST request */
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = $_POST["username"];
        $password = $_POST["password"];

        /* Validate the username */
        if (!validateUsername($username)) {
            $response = array('success' => false);
        } else {
            /* Check if the username already exists in the database */
            $stmt = $conn->prepare("SELECT * FROM users WHERE nom = :username");
            $stmt->bindParam('username', $username, PDO::PARAM_STR);
            $stmt->execute();

            if ($stmt->rowCount() > 0) {
                $response = array('success' => false);
            } else {
                /* Insert the new user into the database */
                $stmt = $conn->prepare("INSERT INTO users (nom, password) VALUES (:username, :password)");
                $stmt->bindParam('username', $username, PDO::PARAM_STR);
                $stmt->bindParam('password', $password, PDO::PARAM_STR);
                $stmt->execute();

                $response = array('success' => true);
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    } else {
        echo "Invalid request method";
    }
} catch (PDOException $e) {
    $response = array('success' => false, 'error' => $e->getMessage());

    header('Content-Type: application/json');
    echo json_encode($response);
}

$conn = null;

function validateUsername($username)
{
    $emailPattern = '/^[^\s@]+@ies-sabadell\.cat$/';
    return preg_match($emailPattern, $username);
}

?>
