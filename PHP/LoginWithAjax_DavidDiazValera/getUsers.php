<?php
require_once 'config.php';

try {
    global $servername, $username, $password, $dbname;
    /* Establish the connection to the database */
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    /* set the PDO error mode to exception */
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    /* prepare and execute the query to get all the users */

    $sql = $conn->prepare("SELECT nom, password FROM users;");
    $sql->execute();

    /* retrieve the data */
    $result = $sql->fetchAll(PDO::FETCH_ASSOC);

    /* ToDo: return data to the AJAX request */
    print_r(json_encode($result));

} catch (PDOException $e) {
    $sql = "";
    print_r(json_encode($sql . $e->getMessage()));
}
// close the PDO Connection
$conn = null;
?>
