$(document).ready(function(){
    $("#lout").click(function () {
        $.ajax({
            method: "POST",
            url: "logout.php",
            dataType: "json",
            success: function(data){
                console.log(data);
                if(data.success){
                    $("#lout").attr("hidden","true");
                    $('#email').removeAttr('hidden');
                    $('#pass').removeAttr('hidden');
                    $('#lo').removeAttr('hidden');
                    $('#re').removeAttr('hidden');
                }
            },
            error: function(jqXHR, textStatus, error){
                alert("Error: "+textStatus+" "+error);
            }
        });
    });
});