$(document).ready(function(){
    $("#re").click(function () {
        $.ajax({
            method: "POST",
            url: "register.php",
            data:{
                "user": $("#user").val(),
                "password": $("#password").val()
            },
            dataType: "json",
            success: function(data){
                $("#info").empty();
                if(data==0){
                    $("#info").attr("class","m-5 text-danger");
                    $("#info").append("The username must be a valid @ies-sabadell.cat account");
                }else if(data==1){
                    $("#info").attr("class","m-5 text-danger");
                    $("#info").append("The username is already registered");
                }else{
                    $("#info").attr("class","m-5 text-success");
                    $("#info").append("User registered successfully");
                }
                window.stop();
            },
            error: function(jqXHR, textStatus, error){
                alert("Error: "+textStatus+" "+error);
            }
        });
    });
});
