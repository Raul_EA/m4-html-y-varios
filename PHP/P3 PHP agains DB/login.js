function sessionActive(){
    $.ajax({
        method: "GET",
        url:"session.php",
        success: function (data) {
            if(data !="" & data){
                $("#email").attr("hidden","true");
                $("#pass").attr("hidden","true");
                $("#lo").attr("hidden","true");
                $("#re").attr("hidden","true");
                $('#lout').removeAttr('hidden');
            }
        },
        error: function (jqXHR, textStatus, error){
            alert("Error: "+ textStatus+" " + error);
        }
    })
}

$(document).ready(function(){
    $("#lo").click(function () {
        $.ajax({
            method: "POST",
            url: "login.php",
            data:{
                "user": $("#user").val(),
                "password": $("#password").val()
            },
            dataType: "json",
            success: function(data){
                $("#info").empty();
                if(data==true){
                    $("#email").attr("hidden","true");
                    $("#pass").attr("hidden","true");
                    $("#lo").attr("hidden","true");
                    $("#re").attr("hidden","true");
                    $('#lout').removeAttr('hidden');
                    $("#info").attr("class","m-5 text-success");
                    $("#info").append("You have successfully logged in");
                }else {
                    $("#info").attr("class","m-5 text-danger");
                    $("#info").append("Username and/or password incorrect");
                }
            },
            error: function(jqXHR, textStatus, error){
                alert("Error: "+textStatus+" "+error);
            }
        });
    });
    sessionActive();
});
