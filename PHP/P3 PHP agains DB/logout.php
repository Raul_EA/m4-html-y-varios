<?php
    session_start();
    $_SESSION["active"] = false;
    session_destroy();

    $res = array('success' => true);

    echo json_encode($res);
?>