$(document).ready(function(){
    $("#us").click(function () {
        $.ajax({
            method: "POST",
            url: "getUsers.php",
            data:{
                "id":$("#input").val(),
                "nom":$("#input").val(),
                "password":$("#input").val()
            },
            dataType: "json",
            success: function (data) {
                $("#tableta").empty();

                let head= "<thead><tr><th scope=\"col\">#</th><th scope=\"col\">Email</th></tr></thead><tbody id=\"tableBody\"></tbody>";
                $("#tableta").append(head);

                for (let i=0;i<data.length;i++){
                    let user=data[i];
                    let row= "<tr><td>"+user.id+"</td><td>"+user.nom+"</td></tr>"
                    $("#tableBody").append(row);
                }
            },
            error:function (jqXHR, texStatus, error){
                alert("Error:" + texStatus + " " + error);
            }
        });
    });
});
