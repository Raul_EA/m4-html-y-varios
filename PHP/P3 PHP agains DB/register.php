<?php
require_once 'config.php';
$a=$_POST["user"];
$b=$_POST["password"];
$pc = strpos($a, "@ies-sabadell.cat");
if($pc==false){
    print_r(json_encode(0));
}else {

    global $servername, $username, $password, $dbname;
    try {
        /* Establish the connection to the database */
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        /* set the PDO error mode to exception */
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        /* prepare and execute the query to know if the user with the given password is registered */
        $sql = $conn->prepare("select nom from users WHERE nom LIKE :nom");
        /* bind user */
        $sql->bindParam(':nom', $a, PDO::PARAM_STR);
        /* execute query */
        $sql->execute();
        /* retrieve the data */
        $result = $sql->fetch(PDO::FETCH_ASSOC);
        $sql->rowCount();
        if( $sql->rowCount()==1){
            print_r(json_encode(1));
        }else{
            $sql = $conn->prepare("insert into users (nom, password) VALUES (:nom,:password)");
            $sql->bindParam(':nom', $a, PDO::PARAM_STR);
            $sql->bindParam(':password', $b, PDO::PARAM_STR);
            $sql->execute();
            print_r(json_encode(2));
        }
    } catch (PDOException $e) {
        print_r(json_encode($e->getMessage()));
    }

// close the PDO Connection
    $conn = null;
}
?>
