<?php
require_once 'config.php';
session_start();
$_SESSION["active"]= false;

$a=$_POST["user"];
$b=$_POST["password"];

global $servername, $username, $password, $dbname;
try {
/* Establish the connection to the database */
$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
/* set the PDO error mode to exception */
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

/* prepare and execute the query to know if the user with the given password is registered */
$sql = $conn->prepare("select nom,password from users WHERE nom LIKE :nom AND password LIKE :password");
/* bind user */
$sql->bindParam(':nom', $a, PDO::PARAM_STR);
/* bind password */
$sql->bindParam(':password', $b, PDO::PARAM_STR);
/* execute query */
$sql->execute();
if ($sql->rowCount()>0){
   $res = true;
   $_SESSION["active"]= true;
}else{
   $res = false;
}
print_r(json_encode($res));
} catch (PDOException $e) {
   print_r(json_encode($e->getMessage()));
}

// close the PDO Connection
$conn = null;
?>
