<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="utf-8">
    <title>Exercise 2</title>
    <meta name="description" content="Exercise 2">
    <meta name="author" content="Raul Edo Andres">
    <meta name="keyword" content="HTML5,PHP">
    <style>
        table{
            font-size: 50px;
        }
    </style>
</head>
<body>
<?php
$base=rand(1,20);
$height=rand(1,20);
$area=($base*$height)/2;
echo "
            <table>
                <tr>
                    <th>Triangle</th>
                </tr>
                <tr>
                    <td>The base is: $base</td>
                </tr>
                <tr>
                    <td>The height is: $height</td>
                </tr>
                <tr>
                    <td>The area of the triangle is $area</td>
                </tr>
            </table>
    ";
?>
</body>
</html>