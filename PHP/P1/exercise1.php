<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="utf-8">
    <title>Exercise 1</title>
    <meta name="description" content="Exercise 1">
    <meta name="author" content="Raul Edo Andres">
    <meta name="keyword" content="HTML5,PHP">
    <style>
        table,th, td{
            border: 10px solid black;
            border-collapse: collapse;
            font-size: 50px;
            margin: 10px;
        }
    </style>
</head>
<body>
    <?php
        $name="Raul Marti";
        $surnames="Edo Andres";
        $grupo="1rDAM-C";
        echo "
            <table>
                <tr>
                    <th>$grupo</th>
                </tr>
                <tr>
                    <td>$name</td>
                </tr>
                <tr>
                    <td>$surnames</td>
                </tr>
            </table>"
    ?>
</body>
</html>