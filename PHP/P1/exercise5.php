<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="utf-8">
    <title>Exercise 5</title>
    <meta name="description" content="Exercise 5">
    <meta name="author" content="Raul Edo Andres">
    <meta name="keyword" content="HTML5,PHP">
    <style>
        li{
            font-size: 20px;
        }
    </style>
</head>
<body>
<?php
$array = array(
    "Hormiguero",
    "Salvame",
    "La isla",
    "Frank de la jungla"
);
echo"
<ol>
 <li>$array[0]</li>
 <li>$array[1]</li>
 <li>$array[2]</li>
 <li>$array[3]</li>
</ol>
";
?>
</body>
</html>