<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="utf-8">
    <title>Exercise 3</title>
    <meta name="description" content="Exercise 3">
    <meta name="author" content="Raul Edo Andres">
    <meta name="keyword" content="HTML5,PHP">
    <style>
        p{
            font-size: 50px;
        }
    </style>
</head>
<body>
<?php
$number=rand(1,100);
echo "<p>The number generated is: $number</p>";
if ($number % 3 == 0) {
    echo "<p>The number is multiple of 3</p>";
} else {
    echo "<p>The number is not multiple of 3</p>";
}
?>
</body>
</html>