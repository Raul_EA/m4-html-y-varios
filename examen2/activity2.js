let para = document.createElement("p");

let num1 = parseInt(prompt("Introduce a number between -100 and 100:"));
while(num1<(-100) || num1>100){
    window.alert("Introduced number is incorrect");
    num1 = parseInt(prompt("Introduce a number between -100 and 100:"));
}
let num2 = parseInt(prompt("Introduce a number between -50 and 50:"));
while(num2<(-50) || num2>50){
    window.alert("Introduced number is incorrect");
    num2 = parseInt(prompt("Introduce a number between -50 and 50:"));
}
let op = String(prompt("Introduce the symbol of the operator (+,-,*,/)"));
while(op!='+' && op!='-' && op!='*' && op!='/'){
    window.alert("Introduced symbol is incorrect");
    op = String(prompt("Introduce the symbol of the operator (+,-,*,/)"));
}
let a=0;
if(op=='+'){
    a=num1+num2;
}
else if(op=='-'){
    a=num1-num2;
}
else if(op=='*'){
    a=num1*num2;
}
else if(op=='/'){
    a=num1/num2;
}
if(num2==0 && op=='/'){
    const s = "Invalid operation";
    document.body.appendChild(para);
    para.innerHTML=s;
}else if(a>0){
    const x = String(a);
    document.body.appendChild(para);
    para.innerHTML=x;
}
else if(a<0){
    const x = String(a);
    document.body.appendChild(para);
    para.innerHTML=x;
}else if(a==0){
    let x = String(a);
    para.innerHTML=x;
    para.style.color="black";
    para.style.backgroundColor="yellow";
    document.body.appendChild(para);
}
