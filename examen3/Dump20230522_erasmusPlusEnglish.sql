CREATE DATABASE  IF NOT EXISTS `erasmusPlusEnglish`;
USE `erasmusPlusEnglish`;
-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: erasmusPlusEnglish
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `erasmusPlusEnglish`
--

DROP TABLE IF EXISTS `erasmusPlusEnglish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `erasmusPlusEnglish` (
  `idstudent` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `surnames` varchar(45) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `destinations` text,
  `speaksEnglish` varchar(50) NOT NULL,
  PRIMARY KEY (`idstudent`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `erasmusPlusEnglish`
--

LOCK TABLES `erasmusPlusEnglish` WRITE;
/*!40000 ALTER TABLE `erasmusPlusEnglish` DISABLE KEYS */;
INSERT INTO `erasmusPlusEnglish` VALUES (1,'Pep','García Sala','pep@ies-sabadell.cat','{\"opcio1\":"Dublin",\"opcio2\":"Porto"}','YES'),
(2,'Ramon','Sánchez Gomis','ramon@ies-sabadell.cat','NULL','YES'),
(3,'Laia','Verdaguer Sabater','laia@ies-sabadell.cat','NULL','NO'),
(4,'Jesús','Smith Fidalgo','jesus@ies-sabadell.cat','{\"opcio1\":"Scopje",\"opcio2\":"Split"}','YES'),
(5,'José','López Fernández','jose@ies-sabadell.cat','NULL','NO'),
(6,'Manuel','García Molina','manuel@ies-sabadell.cat','{\"opcio1\":"Nicosia",\"opcio2\":"Split"}','NO'),
(7,'Nerea','Puig Ramon','nerea@ies-sabadell.cat','NULL','YES');
/*!40000 ALTER TABLE `erasmusPlusEnglish` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-21 12:41:23
