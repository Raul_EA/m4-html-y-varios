<?php
require_once 'config.php';
global $servername, $username, $password, $dbname;

$a=$_POST["email"];

try {
    /* Establish the connection to the database */
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    /* set the PDO error mode to exception */
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = $conn->prepare("select * from erasmusplusenglish where email=:a");
    /* bind user */
    $sql->bindParam(':a', $a, PDO::PARAM_STR);
    $sql->execute();
    if ($sql->rowCount()>0){
        $sql = $conn->prepare("select * from erasmusplusenglish where email=:a AND speaksEnglish='YES'");
        /* bind user */
        $sql->bindParam(':a', $a, PDO::PARAM_STR);
        $sql->execute();
        if($sql->rowCount()>0){
            $sql = $conn->prepare("select destinations from erasmusplusenglish where email=:a");
            $sql->bindParam(':a', $a, PDO::PARAM_STR);
            $sql->execute();
            $res=$sql->fetchAll(PDO::FETCH_ASSOC);
        }else {
            $res = 2;
        }
    }else{
        $res=1;
    }
    print_r(json_encode($res));
} catch (PDOException $e) {
    //print_r(json_encode($e->getMessage()));
}

// close the PDO Connection
$conn = null;
?>
