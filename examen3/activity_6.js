$(document).ready(function(){
    $("#gc").click(function () {
        $.ajax({
            method: "POST",
            url: "activity_6_gc.php",
            data:{
                "name":$("#input").val(),
                "surnames":$("#input").val()},
            dataType: "json",
            success: function (data) {
                $("#tableta").empty();

                let head= "<thead><tr><th scope=\"col\">Name</th><th scope=\"col\">Surnames</th></tr></thead><tbody id=\"tableBody\"></tbody>";
                $("#tableta").append(head);

                for (let i=0;i<data.length;i++){
                    let user=data[i];
                    let row= "<tr><td>"+data[i].name+"</td><td>"+data[i].surnames+"</td></tr>"
                    $("#tableBody").append(row);
                }
            },
            error:function (jqXHR, texStatus, error){
                alert("Error:" + texStatus + " " + error);
            }
        });
    });
    $("#cs").click(function () {
        $.ajax({
            method: "POST",
            url: "activity_6_cs.php",
            data: {
                "email": $("#email").val()
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                if(data===1)
                    $("#inf").append("Student not found")
                else if (data===2)
                    $("#inf").append("The student is not a candidate")
                else{
                    let a = "This student is a candidate and the destinations selected are "+data[0].destination;
                    $("#inf").append(a)
                }
            },
            error:function (jqXHR, texStatus, error){
                alert("Error:" + texStatus + " " + error);
            }
        });
    });
});
