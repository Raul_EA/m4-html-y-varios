$(document).ready(function(){
    $("#res").click(function () {
        $.ajax({
            method: "POST",
            url: "activity_5.php",
            data:{
                "n1": $("#n1").val(),
                "n2": $("#n2").val(),
                "op": $("#op").val()
            },
            dataType: "text",
            success: function(data){
                console.log(data)
                $("#tres").empty();
                $("#tres").append("The result is: "+data);
            },
            error: function(jqXHR, textStatus, error){
                alert("Error: "+textStatus+" "+error);
            }
        });
    });
});
