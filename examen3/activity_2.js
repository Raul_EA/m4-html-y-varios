document.documentElement.setAttribute("data-theme", "light");
function toggleTheme() {
    let text = document.documentElement.getAttribute("data-theme");
    if(text==="light"){
        document.documentElement.setAttribute("data-theme", "dark");
    }else{
        document.documentElement.setAttribute("data-theme", "light");
    }
}
