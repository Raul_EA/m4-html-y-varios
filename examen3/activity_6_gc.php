<?php
require_once 'config.php';
global $servername, $username, $password, $dbname;
try {
    /* Establish the connection to the database */
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    /* set the PDO error mode to exception */
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /* prepare and execute the query to get all the users */
    $sql = $conn->prepare("select name,surnames from erasmusplusenglish where speaksEnglish='YES'");
    $sql->execute();

    /* retrieve the data */
    $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    print_r(json_encode($result));

} catch (PDOException $e) {
    //print_r(json_encode($e->getMessage()));
}

// close the PDO Connection
$conn = null;
?>